# Analytics Vidhya Big Mart Sales

Repositório para os códigos associados à competição Big Mart Sales from Analytics Vidhya. A competição envolve prever os preços de casas com base em características estruturais e do terreno.
URL: https://datahack.analyticsvidhya.com/contest/practice-problem-big-mart-sales-iii/

# Tecnologias utilizadas:
- Pandas e Numpy (Manipulação de dados)
- Matplotlib e Seaborn (Visualização de dados)
- Sklearn (Machine Learning)