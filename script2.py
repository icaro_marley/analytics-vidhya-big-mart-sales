# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 19:09:47 2018

@author: icaromarley5

Checar correlações 

# outlier removal!

	identifier nao é correlated, existem counfounding variables
	comparar com o validaiton
	escolher parametros, etc
	treinar em tudo
	submeter 
	finalizar projeto
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor

pd.options.display.max_rows = 1000
pd.options.display.max_columns = 1000
train_data_path = 'Train_UWu5bXk.csv'
test_data_path = 'Test_u94Q5KV.csv'

pred_data_path = 'predictions.csv'

df_train = pd.read_csv(train_data_path)
df_test = pd.read_csv(test_data_path)

set(df_train.columns) == set(df_test.columns)
# target variable is missing
df_test['Item_Outlet_Sales'] = np.NaN

df = pd.concat([df_train,df_test])

random_state = 0
target = 'Item_Outlet_Sales'
columns = [column for column in df.columns if column != target]

'''
The data scientists at BigMart have collected 2013 sales data for 1559 products across 10 stores in different cities. Also, certain attributes of each product and store have been defined. The aim is to build a predictive model and find out the sales of each product at a particular store.
Using this model, BigMart will try to understand the properties of products and stores which play a key role in increasing sales.
'''
'''
checking assumptions
'''

# rows are uncorrelated
df.set_index(['Outlet_Identifier','Item_Identifier'],inplace=True)
df.index.unique().shape[0] == df.shape[0]
df.reset_index(inplace=True)

# dominance of nulls 
df.isnull().sum()/df.shape[0]
'''
all variables are usable
'''

df['Item_Visibility'].unique()
df[df['Item_Weight'] <= 0].shape # no negative weight_values

# check for typos
df['Item_Fat_Content'].unique() # repeated values
df['Item_Fat_Content'] = df['Item_Fat_Content'].replace({
    'Low Fat':0,
    'low fat':0,
    'LF':0,
    'reg':1,
    'Regular':1,
})

df['Item_Visibility'].unique()
df[df['Item_Visibility'] <= 0].shape # presence of zero or negative values
df[df['Item_Visibility'] > 1].shape # no negative above 1 values
# setting those values to zero
df.loc[df['Item_Visibility']<=0,'Item_Visibility'] = np.NaN

df['Item_Type'].unique()
# no repeated values

df['Item_MRP'].unique()
df[df['Item_MRP']<=0].shape # no zero or negative values

df['Item_Identifier'].unique()
'''
The string on Item_Identifier were generated to explain the items
The format is 2 leters for category + 1 letter + 2 digits
FD,DR,NC means FOOD, DRINK and NON CONSUMABLE
since there is little information about the data and its columns
I'll extract all information inside item and outlet identifiers
I can check if this information contains any patterns later
If those variables are just indices, I'll discard them
'''
# coding those variables
df['item_type'] = df['Item_Identifier'].apply(lambda x:x[:2])
df['item_type'].unique()
df['item_letter'] = df['Item_Identifier'].apply(lambda x:x[2])
df['item_letter'].unique()
df['item_digit'] = df['Item_Identifier'].apply(lambda x:x[3:]).astype(int)
df['item_digit'].unique()
columns = columns + ['item_type','item_letter','item_digit']
df['Outlet_Establishment_Year'].min() # min value inside possible range
df['Outlet_Establishment_Year'].max() # max value inside possible range

df['Outlet_Size'].unique()
# convert to int
df['Outlet_Size'] = df['Outlet_Size'].replace({
    'Medium':2,
    'Small':1,
    'High':3
})

df['Outlet_Location_Type'].unique()
# coding to int
df['Outlet_Location_Type'] = df['Outlet_Location_Type'].replace({
    'Tier 1':1,
    'Tier 2':2,
    'Tier 3':3,        
})

df['Outlet_Type'].unique()
# coding to int
df['Outlet_Type'] = df['Outlet_Type'].replace({
   'Supermarket Type1':1,
   'Supermarket Type2':2,
   'Grocery Store':0,
   'Supermarket Type3':3,        
})

df['Item_Outlet_Sales'].dtype
df['Item_Outlet_Sales'].min() <=0 # no less than zero values

df['Outlet_Identifier'].unique()
'''
Coded as OUT for outlet and a 3 digit number
'''
df['outlet_digit'] = df['Outlet_Identifier'].apply(lambda x:x[3:]).astype(int)
df['outlet_digit'].unique()
columns.append('outlet_digit')


(df.groupby('Outlet_Identifier')['Outlet_Type'].unique().apply(len) > 1).any()
(df.groupby('Outlet_Identifier')['Outlet_Establishment_Year'].unique().apply(len) > 1).any()
(df.groupby('Outlet_Identifier')['Outlet_Location_Type'].unique().apply(len) > 1).any()
(df.groupby('Outlet_Identifier')['Outlet_Size'].unique().apply(len) > 1).any()
# no outlet have more than one type, year, location, size

(df.groupby('Item_Identifier')['Item_Weight'].unique().apply(len) > 1).any()
# some items have more than one weight
# this can happen mostly due to differences in weight calculation or different item brands
(df.groupby('Item_Identifier')['Item_Fat_Content'].unique().apply(len) > 1).any()
(df.groupby('Item_Identifier')['Item_Type'].unique().apply(len) > 1).any()
# no item have more than one fat content, type

# splitting train and submission datasets
df_train = df.iloc[:df_train.shape[0]]
df_test = df.iloc[df_train.shape[0]:]
del df

# split
from sklearn.model_selection import train_test_split

X_train,X_val,y_train,y_val = train_test_split(df_train[columns],df_train[target],random_state=random_state,test_size=0.33)

X_train[target] = y_train
X_val[target] = y_val
del y_train,y_val,df_train
X = pd.concat([X_train,X_val],axis=0)

# analysis
X_train.describe()


X_train.isnull().sum() / X_train.shape[0]
'''
Outlet_Size 0.279335
Item_Weight 0.170928
Item_Visibility 0.061996
'''

sns.countplot('Outlet_Identifier',data=X_train)
plt.xticks(rotation=35)
plt.show()
'''
10 and 19 have more data, others have almost the same quantity
'''

sns.countplot(X_train['Item_Identifier'].value_counts())
plt.show()
sns.countplot(X_train['item_type'])
plt.show()
sns.distplot(X_train['item_digit'])
plt.show()
sns.countplot(X_train['item_letter'])
plt.show()
'''
most items have more than one entries. most have 3-4 entries
item digits and letters are well spread across the dataset
FD is the most commom type
'''
# addding variable is_consumable
X_train['is_consumable'] = (X_train['item_type'] == 'NC').astype(int)
columns.append('is_consumable')
sns.countplot(X_train['is_consumable'])
plt.show()

# right skewed
sns.countplot('Item_Fat_Content',data=X_train)
plt.show()
'''
less items with higher fat content
almost have normal distribution
'''
sns.countplot(X_train['Item_Type'])
plt.xticks(rotation=90)
plt.show()
'''
snack foods and fruit and vegetables have more rows
sea food and breakfest are the types with less rows
would be interesting to join some less frequent and similar types
but i'll check some correlations with the dependant variable first
'''
sns.distplot(X_train['Item_MRP'])
plt.show()
'''
trimodal, modes around 45 100 and 175
'''
sns.countplot(X_train['Outlet_Establishment_Year'])
plt.show()
'''
the data has more stores around created around 1995
has few stores created around 1998
'''
sns.countplot(X_train['Outlet_Location_Type'])
plt.show()
'''
type 3 are more frequent than type 2 which are more frequent than type 1
'''

sns.countplot(X_train['Outlet_Type'])
plt.show()
'''
type 1 are more frequent
other values aren't frequent
check variable to see if is possible to join less frequent values
'''
sns.distplot(X_train['Item_Outlet_Sales'])
plt.show()
'''
right skewed
'''

sns.countplot(X_train['Outlet_Size'])
plt.show()
'''
outlets of size 3 are less frequent
'''
X_train['Item_Visibility'].hist()
plt.title('Item_Visibility')
plt.show()
'''
right skewed
median is a better option for imputation, since is resistant to outliers
'''
X_train['Item_Weight'].hist()
plt.title('Item_Visibility')
plt.show()
'''
items around 5 10.5 13 and 21 weight have less counts
since there aren't many outliers can be filled with mean
'''
# data imputation
'''
most common for 'Outlet_Size' by  'Outlet_Type'
mean for Item_Weight by Item_Type
mean for Item_Visibility by Outlet_Identifier
'''
size_counts_by_type = X_train.groupby('Outlet_Type')['Outlet_Size'].value_counts()
def fill_outlet_size_by_type(row): # most common
    if not pd.isnull(row['Outlet_Size']):
        return row['Outlet_Size']
    return size_counts_by_type[row['Outlet_Type']].index[0]
X_train['Outlet_Size'] = X_train.apply(fill_outlet_size_by_type,axis=1)
X_val['Outlet_Size'] = X_val.apply(fill_outlet_size_by_type,axis=1)

weight_mean_by_type = X_train.groupby('Item_Type')['Item_Weight'].mean()
def fill_item_weight_by_type(row): # most common
    if not pd.isnull(row['Item_Weight']):
        return row['Item_Weight']
    return weight_mean_by_type[row['Item_Type']]
X_train['Item_Weight'] = X_train.apply(fill_item_weight_by_type,axis=1)
X_val['Item_Weight'] = X_val.apply(fill_item_weight_by_type,axis=1)

visibility_mean_by_identifier = X_train.groupby('Outlet_Identifier')['Item_Visibility'].mean()
def fill_item_visibility_by_identifier(row): # most common
    if not pd.isnull(row['Item_Visibility']):
        return row['Item_Visibility']
    return visibility_mean_by_identifier[row['Outlet_Identifier']]
X_train['Item_Visibility'] = X_train.apply(fill_item_visibility_by_identifier,axis=1)
X_val['Item_Visibility'] = X_val.apply(fill_item_visibility_by_identifier,axis=1)


sns.countplot(X_train['Outlet_Size'])
plt.show()
'''
now outlet size 1 is more frequent
'''
sns.distplot(X_train['Item_Weight'])
plt.show()
'''
now high frequency around 12
'''
sns.distplot(X_train['Item_Visibility'])
plt.show()
'''
data skewness was maintened
'''

'''
Data exploration
'''
import scipy.stats as st

def plot_numerical(numerical1,numerical2,data):
    coef,pvalue = st.pearsonr(data[numerical1],data[numerical2])    
    sns.lmplot(x=numerical1,y=numerical2,data=data,scatter_kws={'s':.3})
    plt.title("Pearson {:.2f} pvalue {:.2f}".format(coef,pvalue))
    plt.show()  
    
def plot_multiple_categorical(categorical,numerical,data):
    def plot_violin(categorical,numerical,data,*args,**kwargs):
        separator = categorical.unique()[0]
        numerical = numerical.name
        categorical = categorical.name
        df = data.copy()
        df['all'] = ''
        df[separator] = (df[categorical]==separator).astype(int)
        result = st.pointbiserialr(df[numerical],df[separator])
        ax = sns.violinplot('all',numerical,hue=separator,data=df,
                       split=True,scale='area',width=1,*args,**kwargs)
        counts = df[separator].value_counts()
        ypos = ax.get_ylim()[1] /2 
        y_max = df[numerical].max()
        ax.text(.1,ypos,counts[1],color='red')
        ax.text(ax.get_xlim()[0],y_max/1.1,'PB {:.2f}'.format(result.correlation),color='red')       
        ax.text(ax.get_xlim()[0],y_max/1.2,'α {:.2f}'.format(result.pvalue),color='red')  
        ax.text(0,ax.get_ylim()[0],separator,rotation=90)   
        return ax
    g = sns.FacetGrid(data, col=categorical)
    g.map(plot_violin, categorical, numerical,data=data)
    g.fig.subplots_adjust(wspace=0,left=0,right=.2)
    g.set_titles('')
    g.set_xlabels('')
    for i,ax in enumerate(g.axes.flatten()):
        if i==0:
            ax.set_title(categorical)
            ax.set_frame_on(False)
            ax.xaxis.set_visible(False)
            xmin, xmax = ax.get_xaxis().get_view_interval()
            ymin, ymax = ax.get_yaxis().get_view_interval()
            ax.add_artist(plt.Line2D((xmin, xmin), (ymin, ymax), color='black', linewidth=2))
            continue
        if i == len(g.axes.flatten())-1:
            ax.legend(bbox_to_anchor=(2.3,.5))
        ax.axis('off')
    plt.show()
    
sns.countplot('Item_Fat_Content',hue='item_type',data=X_train)
plt.show()
# all Non Consumable items have 0 fat content

(X_train.groupby('Item_Identifier')['Item_MRP'].unique().apply(len) > 1).any()
mrp_means = X_train.groupby('Item_Identifier')['Item_MRP'].mean()
def compare_mrp(row):
    mean = mrp_means[row['Item_Identifier']]
    if row['Item_MRP'] < mean:
        return 1
    return 0
X_train['bellow_mrp'] = X_train.apply(compare_mrp,axis=1)
columns.append('bellow_mrp')
sns.countplot(X_train['bellow_mrp'])
plt.show()
'''
MRP is store dependant. check if is possible to create a variable with that
if item is bellow Mean MRP
if this variable is true, it means the item is possible sold at lower values than market 
this could rise sales
almost half of the items are bellow the mean mrp
'''

sns.countplot('Item_Fat_Content',hue='item_type',data=X_train)
plt.show()
'''
all non consumables have no fat
most drinks are low fat
food is almost equally distributed
'''

X_train.groupby('Item_Type')['item_type'].value_counts().unstack().plot(kind='bar',stacked=True)
plt.xticks(rotation=90)
plt.show()
'''
all others, health and hygiene, household are non consumable
all baking goods, breads, breakfast, canned, frozen foods,fruits and vegetables, meat, seafood, snack foods, starchy foods are consumable
diary has foods and drinks
'''

plot_multiple_categorical('item_type','Item_Weight',X_train)
'''
weak correlation (positive: NC)
'''
plot_multiple_categorical('item_type','Item_Visibility',X_train)
'''
weak correlation (negative NC, positive FD)
'''
plot_numerical('Item_Visibility','Item_Weight',X_train)
# item visibility has no correlation with item weight
plot_multiple_categorical('Outlet_Type','Item_MRP',X_train)
plot_multiple_categorical('Outlet_Size','Item_MRP',X_train)
plot_multiple_categorical('Outlet_Location_Type','Item_MRP',X_train)
plot_multiple_categorical('Outlet_Identifier','Item_MRP',X_train)
# outlet information has no effect on item mrp
plot_multiple_categorical('item_type','Item_MRP',X_train)
'''
NC has more items between 100-200 mrp
DR has less items with 100 mrp
'''
plot_multiple_categorical('Item_Fat_Content','Item_MRP',X_train)
# item fat doesn't affect mrp
plot_multiple_categorical('item_letter','Item_MRP',X_train)
# item letter affects mrp
plot_multiple_categorical('Item_Type','Item_MRP',X_train)
# item letter and type affects mrp
plot_numerical('Item_Visibility','Item_MRP',X_train)
plot_numerical('Item_Weight','Item_MRP',X_train)
plot_numerical('item_digit','Item_MRP',X_train)
# visibility weight and digit doesn't affect mrp

sns.countplot('Outlet_Type',hue='Outlet_Size',data=X_train)
plt.show()
'''
outlet type 0 have 1 and 2 sizes
outlet type 1 have all sizes
outlet type 2 and 3 have only outlet of size 2
'''

sns.countplot('Outlet_Establishment_Year',hue='Outlet_Type',data=X_train)
plt.show()
'''
outlet type 0 have 1 and 2 sizes
outlet type 1 have all sizes
outlet type 2 and 3 have only outlet of size 2
'''

'''
bivariate analysis
'''
numerical_columns = [
 'Item_Weight',
 'Item_Visibility',
 'Item_MRP', 
 'item_digit',
 'outlet_digit',
]

true_target = target
# is possible to aproximate sale_count by Item_MRP
# other patterns can be discovered trying to model this variable
X_train['sales_count'] = X_train[true_target] / X_train['Item_MRP']

sns.distplot(X_train['sales_count'])
plt.show()
# right seked, with a lot of values next to zero
target = 'sales_count'
columns.remove('Item_MRP')

# correlations
for column in numerical_columns:
    plot_numerical(column,target,X_train) 
'''
Item_Weight no pattern, p-value above 0.05
Item_Visibility weak inserve corr, have lower values for .18 and on, need binnarization
Item_MRP moderate correlation,can be also binarized (values around 50 and 150). This correlation might improve with the adition of other variables
item digit has no correlation
outlet digit has no correlation
'''
# remove variables with no pattern
remove_columns = ['Item_Weight','outlet_digit','item_digit']
columns = [column for column in columns if column not in remove_columns]
# binnarization

binary_categorical_columns = [
   'Item_Fat_Content',
   'bellow_mrp','is_consumable',
]

X_train['all'] = ''
for column in binary_categorical_columns:
    ax = sns.violinplot(y=target,x='all',hue=column,data=X_train,legend=['0','1'],split=True,scale='count')
    result = st.pointbiserialr(X_train[target],X_train[column])
    plt.title('pointb corr {:.2f} p-value {:.2f}'.format(result.correlation,result.pvalue))
    counts = X_train[column].value_counts()
    ypos = ax.get_ylim()[1] /2 
    plt.text(-.1,ypos,counts[0])
    plt.text(.1,ypos,counts[1])        
    plt.show()  

'''
Item_Fat_Content have no effect
bellow_rmp and is_consumable: no correlation 
'''
# removing variables with low correlation
remove_columns = [
    'Item_Fat_Content',
    'bellow_mrp',
    'is_consumable',
]
columns = [column for column in columns if column not in remove_columns]

X_train['Item_Identifier'].unique().shape
sns.countplot(X_train['Item_Identifier'].value_counts())
plt.show()
'''
Item_Identifier has 1547 different values, many unbalanced
using this variable will create a very sparse matrix that will require more data to be useful
'''
# removing variable
columns.remove('Item_Identifier')

categorical_columns = [
   'Item_Type',
   'Outlet_Size',
   'Outlet_Location_Type',
   'Outlet_Type',
   'Outlet_Identifier',
   'item_type','item_letter',
   'Outlet_Establishment_Year',]
for column in categorical_columns:
    plot_multiple_categorical(column,target,X_train)   
'''
Item_Type has almost no correlation
Outlet_Size weak correlation (positive:2,inverse:1)
Outlet_Location_Type weak correlation (negative:1, positive:2)
Outlet_Type moderate correlation (positive:3 negative:0), weak correlation (positive:1)
Outlet_Identifier moderate correlation (inverse:010,019 positive: 027). This correlation might be counfunded
item_type: no correlation
item_letter no correlation
Outlet_Establishment_Year moderate correlation (inverse 1998), weak correlation (1985)
'''
# removing features without correlation
remove_columns = ['Item_Type', 'item_type', 'item_letter']
columns = [column for column in columns if column not in remove_columns]

# extracting features

'''
multivariate anaylsis
'''

def score(y_true,y_pred):
    return np.sqrt(mean_squared_error(y_true,y_pred))

def validate(columns,dummy=False):
    print(columns)
    X_train_ = pd.get_dummies(X_train[columns],drop_first=True)
    y_train_true = X_train[true_target]
    X_val_ = pd.get_dummies(X_val[columns],drop_first=True)
    y_val_true = X_val[true_target]
    
    model = RandomForestRegressor(random_state=random_state)
    model.fit(X_train_,X_train[target])
    y_train_pred = model.predict(X_train_) * X_train['Item_MRP']
    y_val_pred = model.predict(X_val_) * X_val['Item_MRP']
    print("RF RMSE ",score(y_train_true,y_train_pred),score(y_val_true,y_val_pred))
    #print('coefs', model.feature_importances_)
    
    model = GradientBoostingRegressor(random_state=random_state,n_estimators=100)
    model.fit(X_train_,X_train[target])
    y_train_pred = model.predict(X_train_) * X_train['Item_MRP']
    y_val_pred = model.predict(X_val_) * X_val['Item_MRP']
    print("GBT RMSE ",score(y_train_true,y_train_pred),score(y_val_true,y_val_pred))
    #print('coefs', model.feature_importances_)
    if dummy:
        mean = y_train_true.mean()
        y_train_pred = [mean] * len(y_train_true)
        y_val_pred = [mean] * len(y_val_true)
        print("Dummy model (mean) RMSE ",score(y_train_true,y_train_pred),score(y_val_true,y_val_pred))
        
        outlet_means = X_train.groupby('Outlet_Identifier')[target].mean()
        def predict_by_outlet(outlet):
            return outlet_means[outlet]
        y_train_pred = X_train['Outlet_Identifier'].apply(predict_by_outlet) * X_train['Item_MRP']
        y_val_pred = X_val['Outlet_Identifier'].apply(predict_by_outlet) * X_val['Item_MRP']
        print("Dummy model (mean by OutletID) RMSE ",score(y_train_true,y_train_pred),score(y_val_true,y_val_pred))
        
        item_means = X_train.groupby('Item_Type')[target].mean()
        def predict_by_item(outlet):
            return item_means[outlet]
        y_train_pred = X_train['Item_Type'].apply(predict_by_item) * X_train['Item_MRP']
        y_val_pred = X_val['Item_Type'].apply(predict_by_item) * X_val['Item_MRP']
        print("Dummy model (mean by Item Type) RMSE ",score(y_train_true,y_train_pred),score(y_val_true,y_val_pred))


selected_columns = []

for column in columns:
    validate([column])
'''
item visibility when used on the RF model causes overfitting
best variable is OutletIdentifier, but it can be counfunded by other values
Outlet Type is the second best
'''
selected_columns.append('Outlet_Type')

g = sns.FacetGrid(data=X_train, col='Outlet_Type')
g.map(plt.scatter,'Item_Visibility',target)
plt.show()
validate(selected_columns+['Item_Visibility'])
'''
item visibility and outlet type have a pattern but it doesn't improve the score
'''

g = sns.catplot(x="Outlet_Establishment_Year", y=target, col="Outlet_Type",
               data=X_train, kind="violin",share='col',dodge=True)
plt.show()
validate(selected_columns+['Outlet_Establishment_Year'])
# improves score

g = sns.catplot(x="Outlet_Size", y=target, col="Outlet_Type",
               data=X_train, kind="violin",share='col',dodge=True)
plt.show() 
validate(selected_columns+['Outlet_Size'])
#  doesn't improve

g = sns.catplot(x="Outlet_Location_Type", y=target, col="Outlet_Type",
               data=X_train, kind="violin",share='col',dodge=True)
plt.show() 
validate(selected_columns+['Outlet_Location_Type'])
#  doesn't improve

g = sns.catplot(x="Outlet_Identifier", y=target, col="Outlet_Type",
               data=X_train, kind="violin",share='col',dodge=True)
plt.show() 
validate(selected_columns+['Outlet_Identifier']) 
# improves but not by much
 
selected_columns.append('Outlet_Establishment_Year')

g = sns.FacetGrid(data=X_train, col='Outlet_Type',row='Outlet_Establishment_Year')
g.map(plt.scatter,'Item_Visibility',target)
plt.show()
validate(selected_columns+['Item_Visibility'])
# doesn't improve

g = sns.catplot(x="Outlet_Identifier", y=target, col="Outlet_Type",row='Outlet_Establishment_Year',
               data=X_train, kind="violin",share='col',dodge=True)
plt.show() 
validate(selected_columns+['Outlet_Identifier'])
# does'nt improve, the selected columns were the counfunding variables

g = sns.catplot(x="Outlet_Size", y=target, col="Outlet_Type",row='Outlet_Establishment_Year',
               data=X_train, kind="violin",share='col',dodge=True)
plt.show() 
validate(selected_columns+['Outlet_Size'])
# does'nt improve

g = sns.catplot(x="Outlet_Location_Type", y=target, col="Outlet_Type",row='Outlet_Establishment_Year',
               data=X_train, kind="violin",share='col',dodge=True)
plt.show() 
validate(selected_columns+['Outlet_Location_Type'])
# does'nt improve

columns = selected_columns
validate(columns)

# GBT performed better
X_train_ = pd.get_dummies(X_train[columns],drop_first=True)
y_train_true = X_train[true_target]
X_val_ = pd.get_dummies(X_val[columns],drop_first=True)
y_val_true = X_val[true_target]
columns_ = X_train_.columns

model = GradientBoostingRegressor(random_state=random_state,n_estimators=100)
model.fit(X_train_,X_train[target])
y_train_pred = model.predict(X_train_) * X_train['Item_MRP']
y_val_pred = model.predict(X_val_) * X_val['Item_MRP']
print("GBT RMSE ",score(y_train_true,y_train_pred),score(y_val_true,y_val_pred))
    
# feature importances
importances = model.feature_importances_
std = np.std([tree[0].feature_importances_ for tree in model.estimators_],
             axis=0)
indices = np.argsort(importances)[::-1]
columns_ = np.array(columns_)
plt.figure()
plt.title("Feature importances")
plt.bar(columns_[indices], importances[indices],
       color="r", yerr=std[indices], align="center")
plt.xticks(rotation=90)
plt.show()
# Year was the most important feature

y_train_pred = model.predict(X_train_) * X_train['Item_MRP']
plt.scatter(y_train_pred,y_train_true)
plt.ylabel(true_target)
plt.xlabel('Predictions')
plt.show()
y_val_pred = model.predict(X_val_) * X_val['Item_MRP']
plt.scatter(y_val_pred,y_val_true)
plt.ylabel(true_target)
plt.xlabel('Predictions')
plt.show()
# predictions errors got bigger for values above 5000

X_train['pred'] = y_train_pred

g = sns.FacetGrid(data=X_train, row='Outlet_Type')
g.map(plt.scatter,'Outlet_Establishment_Year',true_target,color='b',label='True')
g.map(plt.scatter,'Outlet_Establishment_Year','pred',color='r',label='Predictions')
g.add_legend()
plt.show()
# modest predictions

# submit
# load
df_train = pd.read_csv(train_data_path)
df_test = pd.read_csv(test_data_path)
# process
df_train['sales_count'] = df_train[true_target]/df_train['Item_MRP']
# train
X_train_ = pd.get_dummies(df_train[columns],drop_first=True)
X_test_ = pd.get_dummies(df_test[columns],drop_first=True)
model = GradientBoostingRegressor(random_state=random_state,n_estimators=100)
model.fit(X_train_,df_train[target])
# predictions
df_pred = pd.DataFrame([])
df_pred['Item_Identifier'] = df_test['Item_Identifier']
df_pred['Outlet_Identifier'] = df_test['Outlet_Identifier']
df_pred['Item_Outlet_Sales'] = model.predict(X_test_) * df_test['Item_MRP']
# save 
df_pred.to_csv(pred_data_path,index=False)


# position 251 of 2254 top %12